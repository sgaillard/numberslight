package sam.com.numberslight.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sgaillard on 27/11/2018.
 */
public class NumberItemDetail implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("text")
    private String text;
    @SerializedName("image")
    private String image;

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public String getImage() {
        return image;
    }

    protected NumberItemDetail(Parcel in) {
        name = in.readString();
        text = in.readString();
        image = in.readString();
    }

    public static final Creator<NumberItemDetail> CREATOR = new Creator<NumberItemDetail>() {
        @Override
        public NumberItemDetail createFromParcel(Parcel in) {
            return new NumberItemDetail(in);
        }

        @Override
        public NumberItemDetail[] newArray(int size) {
            return new NumberItemDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(text);
        dest.writeString(image);
    }
}
