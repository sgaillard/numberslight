package sam.com.numberslight.model;

import com.google.gson.annotations.SerializedName;

/**
 * Call NumberItem to avoid the confusing with java.lang.Number
 *
 * Created by sgaillard on 26/11/2018.
 */
public class NumberItem {
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return image;
    }
}
