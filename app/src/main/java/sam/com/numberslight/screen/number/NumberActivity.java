package sam.com.numberslight.screen.number;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import sam.com.numberslight.R;
import sam.com.numberslight.model.NumberItem;
import sam.com.numberslight.screen.number.list.NumberItemAdapter;

/**
 * This app has a single activity for manage 2 fragments with a Master/Detail pattern with
 * two differents configuration depending of the screen orientation: a configuration with two pane displaying
 * list and detail on the same screen and another configuration displaying the list or the detail over it.
 *
 * It is the system (by resources qualifiers) which chose the right configuration.
 *
 * Created by sgaillard on 26/11/2018.
 */
public class NumberActivity extends AppCompatActivity implements NumberItemAdapter.OnItemClickListener {

    private static final String SAVE_SELECTED_ITEM_NAME = "SAVE_SELECTED_ITEM_NAME";

    private boolean mTwoPane;
    private View mNumberMainContainer;
    private View mNumberDetailContainer;
    private String mLastSelectedItemName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);

        mNumberMainContainer = findViewById(R.id.number_main_container);
        mNumberDetailContainer = findViewById(R.id.number_detail_container);
        if (mNumberDetailContainer != null) {
            //The system (by resources qualifiers) chose to use the two pane layout.
            mTwoPane = true;
        }

        FragmentManager manager = getSupportFragmentManager();
        if(savedInstanceState == null) {
            //At first launch we load the list
            manager.beginTransaction()
                    .replace(R.id.number_main_container, NumberListFragment.create(), NumberListFragment.TAG)
                    .commit();
        }
        else {
            //Recreate this activity after a configChange
            mLastSelectedItemName = savedInstanceState.getString(SAVE_SELECTED_ITEM_NAME);

            Fragment detailFragment = manager.findFragmentByTag(NumberDetailFragment.TAG);
            if(detailFragment != null) {
                //Detail screen was displayed, so we need to recreate it in the right container
                manager.popBackStackImmediate(); //Be sure the BackStack is up to date before recreate the DetailFragment
                if(!TextUtils.isEmpty(mLastSelectedItemName)) {
                    showDetailFragment(mLastSelectedItemName);
                }
                else {
                    //Remove the fragment to not display the old unselected item detail
                    manager.beginTransaction()
                            .remove(detailFragment)
                            .commit();
                }
            }

            NumberListFragment listFragment = (NumberListFragment) manager.findFragmentByTag(NumberListFragment.TAG);
            listFragment.selectItem(mLastSelectedItemName);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //Save the last item selected in order to re-select it in the list after rotation if necessary.
        outState.putString(SAVE_SELECTED_ITEM_NAME, mLastSelectedItemName);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if(!mTwoPane && manager.getBackStackEntryCount() > 0) {
            //User back from the detail screen in one pane configuration.
            setActionBarForHomeScreen();

            mLastSelectedItemName = null;
            ((NumberListFragment) manager.findFragmentByTag(NumberListFragment.TAG)).unSelectItem();
        }

        super.onBackPressed();
    }

    @Override
    public void onItemClick(NumberItem item) {
        showDetailFragment(item.getName());
    }

    /**
     * Manage detail fragment to display the data at the right place in screen
     * @param itemName Item index to load and display data
     */
    private void showDetailFragment(String itemName) {
        FragmentManager manager = getSupportFragmentManager();
        mLastSelectedItemName = itemName;

        if(mTwoPane) {
            NumberDetailFragment fragment = (NumberDetailFragment) manager.findFragmentById(mNumberDetailContainer.getId());

            if(fragment == null) {
                //The fragment has never been created
                fragment = NumberDetailFragment.create(itemName);
                manager.beginTransaction()
                        .replace(mNumberDetailContainer.getId(), fragment, NumberDetailFragment.TAG)
                        .commit();
            }
            else {
                //The fragment has already been created, so we re-used the existing instance
                fragment.setItemName(itemName);
            }
        }
        else {
            //In one pane configuration, we always recreate the detail fragment because this last is destroy on the back press
            Fragment fragment = NumberDetailFragment.create(itemName);
            manager.beginTransaction()
                    .replace(mNumberMainContainer.getId(), fragment, NumberDetailFragment.TAG)
                    .addToBackStack(null)
                    .commit();
            setActionBarForDetailScreen();
        }
    }

    private void setActionBarForHomeScreen() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle(R.string.app_name);
        }
    }

    private void setActionBarForDetailScreen() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_item_detail);
        }
    }
}
