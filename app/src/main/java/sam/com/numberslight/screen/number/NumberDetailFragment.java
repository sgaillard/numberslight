package sam.com.numberslight.screen.number;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import sam.com.numberslight.R;
import sam.com.numberslight.model.NumberItemDetail;

/**
 * Used to load and display the data of the selected NumberItem
 *
 * Created by sgaillard on 27/11/2018.
 */
public class NumberDetailFragment extends BaseFragment {
    public static NumberDetailFragment create(String name) {
        //Use a Fabric design pattern in order to try to keep the Bundle keys in the same class
        Bundle arguments = new Bundle();
        arguments.putString(NumberDetailFragment.ITEM_NAME, name);
        NumberDetailFragment fragment = new NumberDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static final String TAG = "NumberDetailFragment";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM ="item";

    @Inject
    NumberPresenter mNumberPresenter;

    private TextView mTextDetail, mTextTitle;
    private ImageView mImageDetail;
    private NumberItemDetail mItemDetail;
    private String itemNameToLoad;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if(savedInstanceState != null) {
            mItemDetail = savedInstanceState.getParcelable(ITEM);
        }
        else if (bundle != null && bundle.containsKey(ITEM_NAME)) {
            setItemName(bundle.getString(ITEM_NAME));
        }
    }

    @Override
    public void onStop() {
        mNumberPresenter.cancelNumberDetailRequest();
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_number_detail, container, false);
        mTextDetail = rootView.findViewById(R.id.text_item_detail);
        mImageDetail = rootView.findViewById(R.id.image_item_detail);
        mTextTitle = rootView.findViewById(R.id.text_item_name);

        if (mItemDetail != null) {
            fill();
        }

        return rootView;
    }

    @Override
    void refreshData() {
        mNumberPresenter.getNumberDetail(itemNameToLoad, numberItemDetail -> {
            mItemDetail = numberItemDetail;
            fill();
            return null;
        }, status -> {
            setErrorOccurs();
            showError(status);
        });
    }

    public void setItemName(String itemName){
        itemNameToLoad = itemName;
        refreshData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(ITEM, mItemDetail);
        super.onSaveInstanceState(outState);
    }

    private void fill() {
        if(mTextDetail != null) {
            //The View has been created (onCreateView already called)
            mTextDetail.setText(mItemDetail.getText());
            mTextTitle.setText(mItemDetail.getName());
            Picasso.get().load(mItemDetail.getImage()).into(mImageDetail);
        }
    }
}
