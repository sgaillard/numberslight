package sam.com.numberslight.screen.number.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import sam.com.numberslight.R;

/**
 * Created by sgaillard on 27/11/2018.
 */
public class NumberItemViewHolder extends RecyclerView.ViewHolder {
    final TextView mNameTextView;
    final ImageView mImageView;

    public NumberItemViewHolder(@NonNull View itemView) {
        super(itemView);
        mNameTextView = itemView.findViewById(R.id.text_item_list);
        mImageView = itemView.findViewById(R.id.image_item_list);
    }

    public void setImageUrl(String imageUrl) {
        Picasso.get().load(imageUrl).into(mImageView);
    }
}
