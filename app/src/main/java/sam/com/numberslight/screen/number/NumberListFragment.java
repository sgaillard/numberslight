package sam.com.numberslight.screen.number;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import sam.com.numberslight.R;
import sam.com.numberslight.screen.number.list.NumberItemAdapter;

/**
 * Used to load and display the list of numbers
 *
 * Created by sgaillard on 27/11/2018.
 */
public class NumberListFragment extends BaseFragment {
    public static NumberListFragment create() {
        //Use a Fabric design pattern in order to try to keep the Bundle keys in the same class
        return new NumberListFragment();
    }

    public static final String TAG = "NumberListFragment";

    @Inject
    NumberPresenter mNumberPresenter;
    private NumberItemAdapter mItemAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_number_list, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.item_list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(mItemAdapter);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItemAdapter = new NumberItemAdapter((NumberItemAdapter.OnItemClickListener) getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!mItemAdapter.hasItems()) {
            //The data never change so we download them only once, except when screen is recreate
            refreshData();
        }
    }

    @Override
    void refreshData() {
        mNumberPresenter.getNumberList((numberItems) -> {
            mItemAdapter.setNumberItemList((List) numberItems);
            return null;
        }, status -> {
            setErrorOccurs();
            showError(status);
        });
    }

    @Override
    public void onStop() {
        mNumberPresenter.cancelNumberListRequest();
        super.onStop();
    }

    public void selectItem(String itemName) {
        mItemAdapter.selectItem(itemName);
    }

    public void unSelectItem() {
        mItemAdapter.unSelectItem();
    }
}
