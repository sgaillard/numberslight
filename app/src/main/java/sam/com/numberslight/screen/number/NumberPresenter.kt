package sam.com.numberslight.screen.number

import android.content.Context
import android.net.ConnectivityManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sam.com.numberslight.model.NumberItem
import sam.com.numberslight.model.NumberItemDetail
import sam.com.numberslight.network.ErrorStatus
import sam.com.numberslight.network.TappticRepository
import javax.inject.Inject



/**
 * In this architecture (MVP), Presenters are used to provide data to the UI (Fragments, Activities ...) and manage network calls and business logic.
 *
 * Created by sgaillard on 26/11/2018.
 */
class NumberPresenter @Inject constructor(
        private val tappticRepository: TappticRepository,
        private val context: Context
) {

    private var numberListCall : Call<List<NumberItem>>? = null
    private var numberDetailCall : Call<NumberItemDetail>? = null

    /**
     * Get the [NumberItem] list from the server and call the callback success or fail
     * @param success Success callback call when we data
     * @param failureListener Fail callback call when something wrong
     */
    fun getNumberList(success: (numberList :List<NumberItem>) -> Unit, failureListener: OnNetworkFailureListener) {
        if(!isOnline()) {
            failureListener.onNetworkFail(ErrorStatus.NO_NETWORK)
            return
        }

        numberListCall = tappticRepository.listNumberItems()
        numberListCall!!.enqueue(object : Callback<List<NumberItem>> {
            override fun onResponse(call: Call<List<NumberItem>>, response: Response<List<NumberItem>>) {
                success(response.body()!!)
            }

            override fun onFailure(call: Call<List<NumberItem>>, t: Throwable) {
                failureListener.onNetworkFail(ErrorStatus.UNKNOWN_ERROR)
            }
        })
    }

    fun cancelNumberListRequest() {
        numberListCall?.cancel()
    }

    /**
     * Get the [NumberItemDetail] from the server and call the callback success or fail
     * @param success Success callback call when we data
     * @param failureListener Fail callback call when something wrong
     */
    fun getNumberDetail(name :String, success: (numberDefailt :NumberItemDetail) -> Unit, failureListener: OnNetworkFailureListener) {
        if(!isOnline()) {
            failureListener.onNetworkFail(ErrorStatus.NO_NETWORK)
            return
        }

        numberDetailCall = tappticRepository.getNumberItem(name)
        numberDetailCall!!.enqueue(object : Callback<NumberItemDetail> {
            override fun onResponse(call: Call<NumberItemDetail>, response: Response<NumberItemDetail>) {
                success(response.body()!!)
            }

            override fun onFailure(call: Call<NumberItemDetail>, t: Throwable) {
                failureListener.onNetworkFail(ErrorStatus.UNKNOWN_ERROR)
            }
        })
    }

    fun cancelNumberDetailRequest() {
        numberDetailCall?.cancel()
    }

    /**
     * Check the local connectivity
     */
    private fun isOnline(): Boolean {
        val cm =  context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    interface OnNetworkFailureListener {
        fun onNetworkFail(status :ErrorStatus)
    }
}