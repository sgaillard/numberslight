package sam.com.numberslight.screen.number.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sam.com.numberslight.R;
import sam.com.numberslight.model.NumberItem;

/**
 * Store and manage the Numbers list and the item click with the selection (before and after that the list is displayed)
 *
 * Created by sgaillard on 27/11/2018.
 */
public class NumberItemAdapter extends RecyclerView.Adapter<NumberItemViewHolder> {
    private List<NumberItem> mNumberItems;
    private OnItemClickListener itemClickListener;

    private View selectedItemView; //This view permit to switch the selection without notify the list
    private String selectedName;

    public NumberItemAdapter(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public NumberItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_content, viewGroup, false);
        return new NumberItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NumberItemViewHolder holder, int i) {
        final NumberItem item = mNumberItems.get(i);
        holder.mNameTextView.setText(item.getName());
        holder.setImageUrl(item.getImageUrl());

        if(TextUtils.equals(item.getName(), selectedName)) {
            //Used to select the correct view in the list managing the recycle view cycle of the RecyclerView.
            holder.itemView.setSelected(true);
            selectedItemView = holder.itemView;
        }
        else if (holder.itemView.isSelected()) {
            holder.itemView.setSelected(false);
            selectedItemView = null;
        }

        holder.itemView.setOnClickListener(v -> onItemClick(v, item));
    }

    private void onItemClick(View v, NumberItem item) {
        unSelectItem();
        selectedItemView = v;
        selectedName = item.getName();
        selectedItemView.setSelected(true);
        if(itemClickListener != null) {
            itemClickListener.onItemClick(item);
        }
    }

    @Override
    public int getItemCount() {
        return (mNumberItems != null) ? mNumberItems.size() : 0;
    }

    public void setNumberItemList(List<NumberItem> mNumberItems) {
        this.mNumberItems = mNumberItems;
        notifyDataSetChanged();
    }

    public void unSelectItem() {
        selectedName = null;
        if(selectedItemView != null) {
            selectedItemView.setSelected(false);
            selectedItemView = null;
        }
    }

    public void selectItem(String itemName) {
        this.selectedName = itemName;
        notifyDataSetChanged();
    }

    public boolean hasItems() {
        return mNumberItems != null && mNumberItems.size() > 0;
    }

    public interface OnItemClickListener {
        void onItemClick(NumberItem item);
    }
}
