package sam.com.numberslight.screen.number;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import dagger.android.support.DaggerFragment;
import sam.com.numberslight.R;
import sam.com.numberslight.network.ErrorStatus;

/**
 * Manage the network errors of fragments query for show a dialog and to permit to the user to retry
 *
 * Created by sgaillard on 28/11/2018.
 */
public abstract class BaseFragment extends DaggerFragment {
    private boolean mMustShowRetryButton;
    private MenuItem mRetryMenuItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    abstract void refreshData();

    protected void setErrorOccurs() {
        if(mRetryMenuItem != null) {
            mRetryMenuItem.setVisible(true);
        }
        else {
            //Use this boolean because the mRetryMenuItem is often create too late and an error occurs before.
            mMustShowRetryButton = true;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        mRetryMenuItem = menu.getItem(0);

        if(mMustShowRetryButton) {
            mRetryMenuItem.setVisible(true);
            mMustShowRetryButton = false;
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.number_menu_retry) {
            mRetryMenuItem.setVisible(false);
            refreshData();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void showError(ErrorStatus status) {
        int idStringMessage;
        if(status == ErrorStatus.NO_NETWORK) {
            idStringMessage = R.string.dialog_error_no_network_message;
        }
        else {
            idStringMessage = R.string.dialog_error_unknown_message;
        }

        new AlertDialog.Builder(getActivity())
                .setMessage(idStringMessage)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_error_button_ok, null)
                .show();
    }
}
