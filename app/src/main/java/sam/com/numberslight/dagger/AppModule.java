package sam.com.numberslight.dagger;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sam.com.numberslight.network.NetworkLogger;
import sam.com.numberslight.network.TappticRepository;

/**
 * Created by sgaillard on 26/11/2018.
 */
@Module
public class AppModule {
    @Provides
    @Singleton
    Retrofit buildRetrofit(Interceptor interceptor) {
        return new Retrofit.Builder()
                .baseUrl("http://dev.tapptic.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .callTimeout(5, TimeUnit.SECONDS)
                        .retryOnConnectionFailure(true)
                        .addNetworkInterceptor(interceptor)
                        .build())
                .build();
    }

    @Provides
    @Singleton
    TappticRepository getTappticRepository(Retrofit retrofit) {
        return retrofit.create(TappticRepository.class);
    }

    @Provides
    Interceptor buildInterceptor() {
        return new NetworkLogger();
    }

    @Provides
    Context getApplicationContext(Application application) {
        return application;
    }
}
