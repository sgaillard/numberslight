package sam.com.numberslight.dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import sam.com.numberslight.screen.number.NumberActivity;
import sam.com.numberslight.screen.number.NumberDetailFragment;
import sam.com.numberslight.screen.number.NumberListFragment;

/**
 * Created by sgaillard on 26/11/2018.
 */
@Module
public abstract class AppBuilder {

    @ContributesAndroidInjector
    abstract NumberDetailFragment bindNumberDetailFragment();

    @ContributesAndroidInjector
    abstract NumberActivity bindNumberActivity();

    @ContributesAndroidInjector
    abstract NumberListFragment bindNumberListFragment();


}
