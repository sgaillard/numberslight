package sam.com.numberslight;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import sam.com.numberslight.dagger.DaggerAppComponent;

/**
 * Created by sgaillard on 26/11/2018.
 */
public class NumbersLightApplication extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
