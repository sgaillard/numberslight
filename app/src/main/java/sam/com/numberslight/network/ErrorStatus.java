package sam.com.numberslight.network;

/**
 * Created by sgaillard on 27/11/2018.
 */
public enum ErrorStatus {
    NO_NETWORK,
    UNKNOWN_ERROR
}
