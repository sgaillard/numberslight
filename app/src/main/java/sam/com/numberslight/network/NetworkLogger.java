package sam.com.numberslight.network;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import sam.com.numberslight.BuildConfig;

/**
 * Log some data from the request and response of a network query.
 * Display this data with INFO level because some devices not display the DEBUG logs.
 * The correct way to manage this logs will be to develop a custom logger using {@link java.util.logging.Logger}
 *
 * Created by sgaillard on 28/11/2018.
 */
public class NetworkLogger implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        if(BuildConfig.DEBUG) {
            Log.i("HttpLog", "Url:"+chain.request().url());
        }
        Response response = chain.proceed(chain.request());

        if(BuildConfig.DEBUG) {
            Log.i("HttpLog", "IsSuccessful:"+response.isSuccessful());
        }
        return response;
    }
}
