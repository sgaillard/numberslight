package sam.com.numberslight.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sam.com.numberslight.model.NumberItem;
import sam.com.numberslight.model.NumberItemDetail;

/**
 * Created by sgaillard on 26/11/2018.
 */
public interface TappticRepository {
    @GET("/test/json.php")
    Call<List<NumberItem>> listNumberItems();

    @GET("/test/json.php")
    Call<NumberItemDetail> getNumberItem(@Query("name") String name);
}
